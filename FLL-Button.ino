/*
  SimpleMQTTClient.ino
  The purpose of this exemple is to illustrate a simple handling of MQTT and Wifi connection.
  Once it connects successfully to a Wifi network and a MQTT broker, it subscribe to a topic and send a message to it.
  It will also send a message delayed 5 seconds later.
*/

#include "EspMQTTClient.h"
#include "secrets.h"

// const char* SSID = "*******";
// const char* PASSWORD = "********";

String region = "notAssigned";

const int buttonPin = 26;
int buttonState;
int lastButtonState = LOW;
int doOnce = false;

unsigned long lastDebounceTime = 0;
const int debounceDelay = 10;

// Change this, but also the last will and client for each button
String buttonName = "button1";

EspMQTTClient client(
  SSID,
  PASSWORD,
  "mqtt.fllscoring.nl",  // MQTT Broker server ip
  "button1",     // Client name that uniquely identify your device
  1883              // The MQTT port, default to 1883. this line can be omitted
);

void setup()
{
  Serial.begin(115200);

  pinMode(buttonPin, INPUT);

  // Optional functionalities of EspMQTTClient
  client.enableDebuggingMessages(); // Enable debugging messages sent to serial output
  client.enableHTTPWebUpdater(); // Enable the web updater. User and password default to values of MQTTUsername and MQTTPassword. These can be overridded with enableHTTPWebUpdater("user", "password").
  client.enableOTA(); // Enable OTA (Over The Air) updates. Password defaults to MQTTPassword. Port is the default OTA port. Can be overridden with enableOTA("password", port).
  client.enableLastWillMessage("fll/button/button1/lastWill", "I am going offline");  // You can activate the retain flag by setting the third parameter to true
}

// This function is called once everything is connected (Wifi and MQTT)
// WARNING : YOU MUST IMPLEMENT IT IF YOU USE EspMQTTClient
void onConnectionEstablished()
{

  // // Publish a message to "mytopic/test"
  client.publish("fll/button/"+buttonName+"/online", "I'm online now!"); // You can activate the retain flag by setting the third parameter to true
  client.publish("fll/button/request", buttonName);

  client.subscribe("fll/button/"+buttonName+"/assign", [](const String& payload) {
    Serial.print("New region assignment: ");
    Serial.println(payload);
    region = payload;
  });


}

void loop()
{
  client.loop();

  int reading = digitalRead(buttonPin);

  if(reading != lastButtonState){
    lastDebounceTime = millis();
  }

  if((millis() - lastDebounceTime) > debounceDelay){
    if(reading != buttonState) {
      buttonState = reading;
    }

    if(buttonState == HIGH && doOnce == false){
      doOnce = true;
      client.publish("fll/timer/"+region+"/start", "start");
    } else if(buttonState == LOW && doOnce == true) {
      doOnce = false;
    }
  }

  lastButtonState = reading;

}
